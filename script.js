window.addEventListener('DOMContentLoaded', () => {
  const words = [
    'carro',
    'casa',
    'amigo',
    'família',
    'comida',
    'escola',
    'trabalho',
    'computador',
    'telefone',
    'internet',
    'música',
    'filme',
    'livro',
    'praia',
    'viagem',
    'dinheiro',
    'tempo',
    'amor',
    'saúde',
    'esporte',
    'arte',
    'natureza',
    'cidade',
    'rua',
    'praça',
    'parque',
    'shopping',
    'supermercado',
    'restaurante',
    'café',
    'chocolate',
    'chave',
    'porta',
    'janela',
    'luz',
    'água',
    'ar',
    'fogo',
    'terra',
    'sol',
    'lua'
  ];
    let score = 0;
    let timer = 60;
    let currentWordIndex = Math.floor(Math.random() * words.length);
  
    const wordElement = document.getElementById('word');
    const inputElement = document.getElementById('input-word');
    const scoreElement = document.getElementById('score');
    const scoreFinal = document.getElementById('score');
    const timerElement = document.getElementById('timer');
  
    function updateWord() {
      currentWordIndex = Math.floor(Math.random() * words.length);
      wordElement.textContent = words[currentWordIndex];
    }
  
    function updateScore() {
      score++;
      scoreElement.textContent = `Pontuação: ${score}`;
    }
  
    function updateTimer() {
      timer--;
      timerElement.textContent = `Tempo Restante: ${timer} segundos`;
  
      if (timer === 0) {
        endGame();
      }
    }
    
    function endGame() {
        inputElement.disabled = true;
        clearInterval(timerInterval);
        scoreFinal.textContent = score;
    }
  
    inputElement.addEventListener('input', () => {
      if (inputElement.value.trim() === words[currentWordIndex]) {
        updateScore();
        inputElement.value = '';
        updateWord();
      }
    });
  
    let timerInterval = setInterval(updateTimer, 1000);
    updateWord();
  });