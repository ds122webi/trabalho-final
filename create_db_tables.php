<?php
require 'db_credentials.php';

// Create connection
$conn = mysqli_connect($servername, $username, $db_password);

if (!$conn) {
    die("Connection failed: " . mysqli_connect_error());
}

// Check connection
if (!$conn) {
    die("Connection failed: " . mysqli_connect_error());
}

// // Create database
// $sql = "CREATE DATABASE $dbname";
// if (mysqli_query($conn, $sql)) {
//     echo "<br>Database created successfully<br>";
// } else {
//     echo "<br>Error creating database: " . mysqli_error($conn);
// }

// Choose database
$sql = "USE $dbname";
if (mysqli_query($conn, $sql)) {
    echo "<br>Database changed successfully<br>";
} else {
    echo "<br>Error changing database: " . mysqli_error($conn);
}

// sql to create table
$sql = "CREATE TABLE $table_users (
    id INT AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(100) NOT NULL,
    password VARCHAR(128) NOT NULL,
    score INT
  )";

if ($conn->query($sql) === TRUE) {
    echo "Tabela 'usuario' criada com sucesso\n";
} else {
    echo "Erro ao criar a tabela 'usuario': " . $conn->error;
}

// if (mysqli_query($conn, $sql)) {
//     echo "<br>Table created successfully<br>";
// } else {
//     echo "<br>Error creating database: " . mysqli_error($conn);
// }

mysqli_close($conn)
?>
