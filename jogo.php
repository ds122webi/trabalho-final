<?php
  require "authenticate.php";
  require "db_functions.php";
  if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $scoreFinal = $_POST["scoreFinal"];
    $conn = connect_db();
    if ($conn->connect_error) {
      die("Falha na conexão com o banco de dados: " . $conn->connect_error);
    }else{
      echo 'CONECTADO';
    }

    // Atualiza o valor do score na tabela table_users
    //$sql = "UPDATE users SET score = '$scoreFinal' WHERE id = $_SESSION['user_id'];";
    $stmt = $conn->prepare("UPDATE users SET score = ? WHERE id = ?");
    $stmt->bind_param("is", $scoreFinal, $user_id);
    $stmt->execute();
    if ($conn->query($sql) === TRUE) {
      echo "Score atualizado com sucesso.";
    } else {
      echo "Erro ao atualizar o score: " . $conn->error;
    }

    // Fecha a conexão com o banco de dados
    $conn->close();
  }
?>

<!DOCTYPE html>
<html>
<head>
  <title>Jogo de Digitar Palavras</title>
  <script src="script.js"></script>
</head>
<body>
  <h1>Jogo de Digitar Palavras</h1>
  <p id="word"></p>
  <input type="text" id="input-word" autofocus>
  <p name="score" id="score">Pontuação: 0</p>
  <p id="timer">Tempo Restante: 60 segundos</p>
  <form action="index1.php" method="post">
    <p name="scoreFinal" id="scoreFinal" value="scoreFinal"></p>
    <input type="submit" name="retornar" value="retornar">
  </form>
  
</body>
</html>