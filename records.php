<?php
require "db_functions.php";
require "authenticate.php";

  
  // Create connection
?>


<!DOCTYPE html>
<html>
<head>
  <title>Jogo de Digitar Palavras</title>
  <link rel="stylesheet" type="text/css" href="style.css">

</head>
<body>
  <h1>Records</h1>
  <div>


    <?php
    $conn = connect_db();
    $sql = "SELECT name, score FROM users ORDER BY score DESC";
    $result = mysqli_query($conn, $sql);

    if(mysqli_num_rows($result) > 0){
        while($row = mysqli_fetch_assoc($result)){
        echo 
        "Nome: " . $row["name"].
        " - Score: " . $row["score"].
        "<br>";


        }
    } else {
        echo "0 Pontuações";
    }
    ?>

  </div>




  <script src="script.js"></script>
</body>
</html>